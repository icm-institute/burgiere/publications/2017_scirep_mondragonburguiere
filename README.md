# 2017_SciRep_MondragonBurguiere

Resources from published article : Bio-inspired benchmark generator for extracellular multi-unit recordings

https://www.nature.com/articles/srep43253

## "MULTIPLE LAYERS" - Folder overview
-----------
This is modified version of Neurocube which allows more than one tetrode to be simulated.Camuñas-Mesa LA, Quiroga RQ. (2013).
A detailed and fast model of extracellular recordings. Neural Comput 25

Electrode parameteres are defined in 
/multielectrode_arrays/NeuroCubeConf.xml
	
Cube parameters are defined in /multielectrode_arrays/cubeConfiguration.m

All other parameters are defined inside the 
main simulation file, which is neurocube_auto.m 
	
NOTE: Adapt cube size according to electrode coordinates

Main file is neurocube_auto

Results are saved in NeuroCube_modified_loop\mat\simulations_multiple_layers\ by date and hour 

It is possible to include notes in the final file that is saved. This is done in the neurocube_auto.m file


## "DATASETS AND CREATION" - FOLDERS OVERVIEW
-----------
CREATION: These matlab files generate the benchmarks described in 
 "Bio-inspired benchmark generator of extracellular multi-unit recordings".   
 It includes the integration of realistic simulations of neural signals + Lf from real signals + collection of artifacts for 2 type of experiments: 

 1 Recordings in the hippocampus in anesthetized rodents.
 2 Recordings in the hippocampus in awake rodents.

 The realistic simulations of neural signals were generated using a modified version of the simulator described in (Luis A. Camuñas-Mesa and Rodrigo Quian Qurioga, 2013.)

 The real signals used for comparison are described in: 
 Anesthetized - (Henze et al., 2009), (Henze et al.2000)
 Awake - (Mizuseki K, 2011)

* TO RUN IN MATLAB
------------------
All the files needed to create the benchmarks are organized in the folder Creation.

+ In Matlab make Creation your current folder, you must add the subfolders into the Matlab path (In Matlab Home/Environment/Set Path/Add with subfolders
+ Run Main.m 

* PARAMETERS
-------------
All the external parameters are described in Parameters_01.m

* OUTPUTS IN DATASET FOLDER
---------
Anesthetized - Vm_AnS_lf_A
Awake -        Vm_AwS_lf_A

+ Grooming artifacts - libGroomingArtifacts.mat
+ Mechanical artifacts -libMechanicalArtifacts.mat
+ Chewing artifacts - libChewingArtifacts.mat
