fprintf('\n     * Experiment 01 selected...'); 
% It contains a list of initialization parameters
%  This conf. fie shows the parameters used in:
%   "Bio-inspired benchmark generator of extracellular multi-unit recording"".   
% Check Files_index file for more informacion 
%  ..................................................................
%                       Anesthetized Experiments
% --> Simulation (AnS)
folder_AnS  = 'Sim_AnS'; 
window_AnS  = [0,10];           % Extracted window from simulation in seconds
elec_AnS    = [1,2,3,4];            % Electrode selection 
p_dt_AnS    = window_AnS;   % Window for periodogram dt = [xmin, xmax]

% --> Real Recording Anesth (AnR)
file_AnR    = 'd533101';          % hc-1 recording folder name
window_AnR  = [70,80];         % Extracted window from recording in seconds
elec_AnR    = [2,3,4,5];           % Electrode selection

fc_AnR      = 600;                    % Cut freq. for highpass filter
fcL_AnR     = 300;                   % Cut fred. for lowpass filter in hz 

p_dt_AnR    = [0,10];              % Window for periodogram dt = [xmin, xmax] in sec
window_lf_AnR   = [50,60];    % Window where the low frequencies will be extracted

% ....................................................................
%                       Awaken Experiments 
%
% --> Simulation (AwS)
folder_AwS  = 'Sim_AwS';       % Folder Awaken Simulation name
window_AwS  = [0,10];           % Extracted window from simulation in seconds
elec_AwS    = [1,2,3,4];           % Electrode selection 

p_dt_AwS    = window_AwS;          % Window for periodogram dt = [xmin, xmax]

% --> Real recording awaken (AwR)
file_AwR    = 'ec013529';          % hc-2 recording folder name
window_AwR  = [701,711];           % Extracted window from recording
elec_AwR    = [10,11,18,19];       % Electrode selection

fc_AwR      = 600;                 % Cut freq. for highpass filter in hz
fcL_AwR     = 300;                 % Cut fred. for lowpass filter in hz 

p_dt_AwR    = [0,10];          % Window for periodogram dt = [xmin, xmax]
window_lf_AwR   = [760 770];       % Window where the low frequencies will be extracted
 
% ....................................................................
%                        Artifacts for Simulations

mode_art = 'ORIGINAL';      % Probability function described by the original 
                            % histogram constructed with the peak-to-peak amplitudes 

artf_rate_AnS = 1 ;      % Art/s
artf_rate_AwS = 10 ;    % Art/s
