function [data_out, artifacts] = addArtefacts_distribution(data_in, samples, conf, mode, varargin)
% data_in : array of all the channels [nchannels nsamples]
% samples : set of artefacts samples{i} 
%       samples{i}.data is a matrix n x maxChannels,
%           with n = artefact length
%       samples{i}.timestamp is a vector (n x 1) of timestamps
%           corresponding to datas
% conf.artf_rate   average rate of artefacts per second 
% conf.samp_rate   sampling rate of datas in sample/s
% mode: can be one of the following values
%       'ORIGINAL' => distribution of the artefacts according to their amplitude
%                   will be similar to the original
%       'FORCED DISTRIB' => distribution of artefacts according to their amplitude
%                   is specified in the argument distribution
% (optional) min_artif_occurence_rate : minimum porcentage of channels with
%               artefacts, if not set, this parameter is equal to 0.8 (80%)
% (optional) distribution => distribution object that defined the
%               distribution that has to be followed by generated artefacts
%
% output: data_out: data with artifacts added
%       artifacts: n_artifacts x 3, first column are id of artifact sample
%               used
%               second column is the start timestamp of the added artefact in ms
%               third column is the end timestamp of the added artefact in ms

    nChannels = size(data_in,1);
    nSamples = size(data_in,2);
    maxChannels = size(samples{1}.data,2);
    
    % set the min_artif_occurence_rate
    if (length(varargin )> 0)
        min_artif_occurence_rate = varargin{1};
    else
        min_artif_occurence_rate = 1;
    end
    
    ts = samples{1}.timestamp;
    old_period = round(100*(ts(end)-ts(1))/(length(ts)-1)); %period in tens of ns
    new_period = round(1e8/conf.samp_rate);
    
    max_artf_length = 0;
    
    % data preparation
    for i = 1 : length(samples)
        %datas = samples{i}.data;
        
        %resampling
        samples{i}.data = resample(samples{i}.data ,old_period ,new_period);
        
        %
        % remove channels with no data and count them:
        %   check for each channel if it is empty, if so move this channel
        %   to the last channel.
        %   Then fill the empty channel at the end with a copy of the first
        %   channels
        %
        count_empty_channels = 0;
        
        artif_data = samples{i}.data;
        for ch = 1:maxChannels  %check for each channel
            if max(artif_data(:, ch-count_empty_channels))== 0    % if no data
                count_empty_channels = count_empty_channels + 1;    % increment count of channels with no data
                artif_data = [artif_data(:, 2:maxChannels)  ...
                                artif_data(:, 1)];              % move empty channel to the end
            end
        end
        
        % replace empty channels (added to the end) by the first channels
        if (count_empty_channels>0)
            artif_data(:, (maxChannels-count_empty_channels+1): maxChannels) = ...
                artif_data(:, 1:count_empty_channels);
        end
        
        samples{i}.data = artif_data;
        
        
        if maxChannels<nChannels
            samples{i}.data = [samples{i}.data zeros(size(samples{i}.data,1), ...
                nChannels-maxChannels)];
        else
            samples{i}.data = samples{i}.data(:, 1:nChannels);
        end
        
        samples{i}.data = samples{i}.data';
        
        max_artf_length = max(size(samples{i}.data,2), max_artf_length);
    end;

    
    duration_samples = nSamples + max_artf_length;      % duration in sample count
    duration_time = duration_samples / conf.samp_rate;  % duration in s
    nb_artf = ceil(conf.artf_rate * duration_time);     % artifact number

    % artefacts is N*d
    %   N = number of artefacts
    %   d = 2, first element is sample id, second element is the index where
    %       artefacts is added
    artifacts = zeros(nb_artf, 3);

    % create the list of artefacts to add
    artifacts(:,1) = ceil(rand(nb_artf, 1) * length(samples));  % artefacts id
    artifacts(:,2) = ceil(rand(nb_artf, 1) * duration_samples) - max_artf_length + 1; % index where to add

    %artf_datas = zeros(length(data_in) + max_artf_length, nChannels);

    % add artifacts to signal
    for i = 1:nb_artf
        artf_datas = samples{artifacts(i,1)}.data; % datas of selected artifact
        len_artf = length(artf_datas);             % duration of artifact
        artifacts(i,3) = artifacts(i,2) + len_artf;
        
        % calculate the number of channel without artefact from min_artif_occurence_rate
        occurence_rate = rand * (1-min_artif_occurence_rate) + min_artif_occurence_rate;
        nb_empty_channels = ceil((1-occurence_rate) * nChannels);
        
        % set nb_empty_channels empty artifacts data
        lst_empty = zeros(nb_empty_channels, 1);
        if nb_empty_channels>0
            for empty = 1:nb_empty_channels
                done = 0;
                while done==0
                    channel = floor(rand * nChannels + 1);
                    if ISEMPTY(find(lst_empty == channel))
                        lst_empty(empty) = channel;
                        done = 1;
                    end
                end
            end
            artf_datas(lst_empty, :) = zeros(1, len_artf);
        end
       
        
        t1 = artifacts(i,2);      % index where to insert first sample
        t2 = t1 + len_artf - 1; % index where to insert last sample
        
        t1_in = max(1, t1);
        t2_in = min(t2, nSamples);
        len_copy = t2_in - t1_in+1;
        
        t1_art = len_artf - min(len_artf, t2) + 1;
        t2_art = t1_art + len_copy - 1;
        
        data_in(:, t1_in:t2_in) = data_in(:, t1_in:t2_in) + ...
            artf_datas(:, t1_art:t2_art);
%         artf_datas(t1:t2) = artf_datas(t1:t2) + samples{i};
    end;
    data_out = data_in;
%     artf_datas = artf_datas(max_artf_length+1:end);
%     data_out = data_in + ones(length(data_in),1) * artf_datas;

    % convert artifact timestamp from sample id to ms
    artifacts(:,2:3) = (artifacts(:,2:3))*1000/conf.samp_rate;
    [~, ids] = sort(artifacts(:,2));
    artifacts = artifacts(ids, :);
end