function [ y ] = GetThr( V, option  )
% Get noise measure for a waveform using a sliding window (epoch).
% Together with a multiplier it can be used as a 
%   threshold for action potential detection. 
% Different methods are available: 1)MD, 2)RMS, 3)P84 etc

% OUTPUT: 

switch (option) 
    case 1      % Mean deviation
        % returns the mean absolute deviation of the values in win
        y = -mad(V'); 
    case 2      % Root-mean square
        y = rms(V');
        display('rms selected')
    case 3      % P84: 84th percentile of the data
        
    otherwise
        warning('This option does not exist')
end
end 