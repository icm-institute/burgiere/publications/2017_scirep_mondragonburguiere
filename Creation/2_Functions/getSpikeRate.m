%% GET THE SPIKE RATE IN A TEMPORAL WINDOW 
function [avg_spk_rate, N, total_avg_spk_rate ] = getSpikeRate (Vm, t, sr,  win, thr, name  )

% INPUT
%   Vm     : in uV
%   t         : in s
%   sr        : in Hz
%   win     : [xlim1, xlim2] in s, or 0 if complete trace
%   thr      : lower thrshold in -uV
%
% OUTPUT

%% ==================================================
%   Detect spikes using 1 lower thrshold
% ====================================================

[nelectrodes, ~] = size (Vm);
N = cell(nelectrodes, 2);
for i=1:1:nelectrodes
    if (size(win) == [1,1]) & (win(1) == 0) % Complete trace
        
        win_duration = t(end);
        [Nspikes,index]      = spike_times(Vm(i,:), thr(i));
        N{i,1} = Nspikes;
        N{i,2} = index;
             
        avg_spk_rate(i,:) = N{i,1} / win_duration;
    
    else                                    % Section of trace
    
        xlim1 = win(1);  % s
        xlim2 = win(2);  % s
        win_duration = xlim2 - xlim1; % Window duration in seconds
       
        [Nspikes,index]       = spike_times(Vm(i,xlim1*sr+1 : xlim2*sr), thr);
        N{i,1} = Nspikes;
        N{i,2} = index;
        
        avg_spk_rate(i,:) =  N{i,1} / win_duration;
    end
end

total_avg_spk_rate = mean(avg_spk_rate);

fprintf (' |=============== Average Spike Rate - during %.2f s  ==================| \n', win_duration)
fprintf ('                  File: %s   \n', name)
for i=1:nelectrodes
    fprintf ('  Extracellular Ch%d: %.2f    Nspk: %d\n', i, avg_spk_rate(i,:), N{i,1})
end
fprintf ('  \n  Average Spike Rate %.2f Electrodes: %16f\n', nelectrodes(end), total_avg_spk_rate)
fprintf ('  \n ')
    