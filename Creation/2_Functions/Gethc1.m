%%  LOADING RECORDING FILES IN uV
function [Vm, t, sr] = Gethc1 (filename, window, elec); 
% This function loads the recordings from the 3_Data folder
% Recordings correspond to crcns hc2 dataset.
% It might be different from voltage conversion of hc1 or other datsets because
% configuration files are were not verified and can't be trusted. 

% If different, change the path to find the file. 
%
% INPUT
%   filename  : Filename found in 2_Recording folder  .dat and .xml are
%               named equally
%             
%   window    : Extracted sim window in (s) determined by [xmin,xmax] 
%               if equal to [0,0] it takes the whole recording. 
%
%   elec      : Electrode selection [el1,el2,...] . If equal to [0]
%               it takes all the electrodes in order. 
% OUTPUT
%   Vm        : in uV
%   t         : in s
%   sr        : in Hz

% Change this path if necesarry 
file_Data = ['4_Data\hc-1\d5331\' filename '.dat'];
file_Xml  = ['4_Data\hc-1\d5331\' filename '.xml'];
exist(file_Data,'file');  % Returns 0 if file does not exists

% Getting sampling frequency 
Par     = LoadPar(file_Xml);  % Reads the .xml File 
sr      = Par.SampleRate;     % In Hz normally 10 or 20 kHz
offset  = Par.Offset;
% Electrodes selected test 
nelec = 4;
%[nelec, ~]  = size(file_Data);

if (size(elec) == [1,1]) & (elec(1) == 0) % All electrodes selected
   
    elec = linspace(1,nelec,nelec);        % Vector contaning all electrode numbers
end

% Window selected test
Vm = [];
Vm01 = [];

Vrange = 200;
if (window == [0,0])                       % Whole recording
    for i = 1:nelec
        data(i,:)  = LoadBinary(file_Data,elec(i));     % Load
        %Vm(i,:)  = (data(i,:) - offset)./(2^11)*Vrange;   % ADC 2 Volt 
        Vm(i,:)  = (data(i,:) - offset)./(2^16-1)*20;   % ADC 2 Volt
    end
else                                       % Extracted window
    xmin = window(1);
    xmax = window(2);

    for i = 1:nelec
        data(i,:)  = LoadBinary(file_Data,elec(i));     % Load
        %Vm01(i,:)  = (data(i,:) - offset)./(2^11)*Vrange;   % ADC 2 Volt  
        Vm01(i,:)  = (data(i,:) - offset)./(2^16-1)*20;   % ADC 2 Volt
        Vm(i,:)    = Vm01(i,(xmin*sr)+1 : (xmax*sr));
        
    end
end
% Time vector 
t=zeros();  % x axis elements (time)
pas=1/sr;   % In the case of 10kHz -> intervals of 0.1 ms between each sample

for i=2:length(Vm(1,:)) % Taking 1 electrode as sample
    t(1)=pas; 
    t(i)=t(i-1)+pas;
end % t is a vector of seconds now

clear tmp Vm01 Par