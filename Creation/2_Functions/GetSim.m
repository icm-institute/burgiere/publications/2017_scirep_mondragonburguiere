%%  LOADING SIMULATION FILES IN uV
function [Vm, t, sr, tmp, tmp2] = GetSim (folder, window, elec)
% This function loads the simulations from simulation folder
% If different, change the path to find the file. 
%
% INPUT
%   folder    : Folder name of simulation file, where the
%               Spikesim_xxx.mat is found. 
%   window    : Extracted sim window in (s) determined by [xmin,xmax] 
%               if equal to [0,0] it takes the whole recording.  
%   elec      : Electrode selection [el1,el2,...] . If equal to [0]
%               it takes all the electrodes in order. 
% OUTPUT
%   Vm        : in uV
%   t         : in s
%   sr        : in Hz
%  tmp        : spike array of sim
%  tmp2       : parameters array of sim 

% Change this path if necesarry 
filename = sprintf ('4_Data\\%s\\Spikesim_001.mat',folder);

if (exist(filename,'file') == 0)  % Returns 0 if file does not exists
    filename = sprintf ('4_Data\\%s\\Spikesim_001.mat',folder);
end
tmp = load( filename);

% Getting sampling frequency 
filename_params = sprintf ('4_Data\\%s\\Parameters_001.mat',folder);
if (exist(filename_params,'file') == 0)  % Returns 0 if file does not exists
    filename_params = sprintf ('4_Data\\%s\\Parameters_001.mat',folder);
end
tmp2 = load(filename_params);
sr= tmp2.Par_sim.sr;                       % Sampling rate in Hz

% Electrodes selected test

if (size(elec) == [1,1]) & (elec(1) == 0) % All electrodes selected  
    [nelec, ~]  = size(tmp.data); 
    elec = linspace(1,nelec,nelec);        % Vector contaning all electrode numbers
   
else
    [ ~, nelec]  = size(elec); 
end

% Window selected test
Vm = [];
Vm01 = [];
if (window == [0,0])                       % Whole recording
    for i = 1:nelec
        Vm(i,:)  = tmp.data(elec(i),:);
    end
else
    xmin = window(1);
    xmax = window(2);

    for i = 1:nelec
        Vm01(i,:)  = tmp.data(elec(i),:);
        Vm(i,:)    = Vm01(i,(xmin*sr)+1 : (xmax*sr));
    end
end
% Time vector 
t=zeros();  % x axis elements (time)
pas=1/sr;   % In the case of 10kHz -> intervals of 0.1 ms between each sample

for i=2:length(Vm(1,:)) % Taking 1 electrode as sample
    t(1)=pas; 
    t(i)=t(i-1)+pas;
end % 

clear Vm01