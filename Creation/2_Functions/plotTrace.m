%%        * Plotting figures *  
function [] = plotTrace (name, Vm,t,N,thr,color, y_lim)
figure
% Title -> Change it for each plot 
uicontrol('Style', 'text', 'String', name, ...
'HorizontalAlignment', 'center', 'Units', 'normalized', ...
'Position', [0 .95 1 .05], 'BackgroundColor', [.8 .8 .8]);

[nelec, ~] = size(Vm);

for i = 1:1: nelec
    H(i)=subplot(nelec,1,i) ;               
    plot(t,Vm(i,:),'Color',color); % Plot in ms 
    ylim(y_lim)    
    hold on 
    
    if (N == 0 && thr == 0)
        plot(t(N{i,2}), Vm(i,N{i,2}),'g.');
        ylim(y_lim)    
        hold on 

        hline = refline([0 thr(i)]);
    end
end

% 
L(1)=xlabel('[s]');
L(1)=ylabel('[uV]');


linkaxes(H, 'x');      % Link all axes in x