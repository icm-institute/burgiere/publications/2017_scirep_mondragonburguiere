function [ sim_with_lf ] = SetLowFreq2Sim( Lf_signal, sr_Lf_signal, sim_signal, sr_sim_signal)

% As input both signals simulated and real must be the same size (in time
% segmens in seconds) and in number of electrodes
%
% sim_with_lf: simulation channels with lf 

% Lf_signal is an extract of low frequency components. 

% sub-sampling if necessary
% Smallest sampling rate chosen
sr_sim_with_lf = min(sr_Lf_signal, sr_sim_signal);

[n,~] = size (sim_signal);  %Nb of channels of sim signal

sim_signal_duration= (size(sim_signal,2))/ sr_sim_signal; % in sec
sim_signal2= zeros(size(sim_signal,1),sim_signal_duration* sr_sim_with_lf); 

flag = 0;
if (sr_Lf_signal < sr_sim_signal)
    for i=1:1:n                      
        s = resample(sim_signal(i,:), sr_Lf_signal, sr_sim_signal);
        sim_signal2(i,:) = s';
        flag = 1;
    end
elseif (sr_Lf_signal > sr_sim_signal)
    
        Lf_signal = resample(Lf_signal, sr_sim_signal, sr_Lf_signal);
    
end;


% Adding low frequency to simulation
% As RESAMPLE function changes the size of the signal, we have to ensure
% that we get the same size as the non_resample signal for the output
% signal

if (flag == 1)
    output_length = min (size(sim_signal2,2), size(Lf_signal,2));
    sim_with_lf = sim_signal2(:,1:output_length) + Lf_signal(:,1:output_length);
else
    output_length = min (size(sim_signal,2), size(Lf_signal,2));
    sim_with_lf = sim_signal(:,1:output_length) + Lf_signal(:,1:output_length);
end


end

