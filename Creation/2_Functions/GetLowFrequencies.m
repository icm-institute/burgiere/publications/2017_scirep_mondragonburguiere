function [Vu] = GetLowFrequencies(file, window, elec, fcut_hz)
% This function gets the frequencies defined in 
% fcut_hz with a low-pass filter.
%
% file:     File name of real-signal example: var='name' (without
% extension (Only for hc2 files)
% window:   Windowd segmen in sec
% elec:     Vector of selected electrode numbers
% fcut_hz:  Fcut of low-pass filter
%
% Vu:       Output. Number of channels equal to input signal 

[Vm_raw, ~, sr] = Gethc2 (file, window, elec);

[ Vm_filt ] = lpFilter( Vm_raw, sr, fcut_hz ); 

% Uncomment to get mean freq of all channels
%Vm_mean_filt = mean(Vm_filt);
%Vu  = Vm_mean_filt*1000; % to uV

Vu  = Vm_filt*1000; % to uV
end 

