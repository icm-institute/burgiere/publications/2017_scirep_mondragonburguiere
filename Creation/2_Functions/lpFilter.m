function [ Vm_filt ] = lpFilter( Vm, sr, fcut_hz )
% Wrap of a low pass filter 
%   Detailed explanation goes here
%% ====================================================
%          Design low pass filter
% ====================================================

fnorm =fcut_hz/(sr/2); % normalized cut off freq, you can change it to any value depending on your requirements

[b,a] = butter(10,fnorm,'low'); % Low pass Butterworth filter of order 10
[nelec, ~] = size(Vm);
for i = 1:1:nelec
Vm_filt(i,:) = filtfilt(b, a, Vm(i,:)); %extracellular 
end

end