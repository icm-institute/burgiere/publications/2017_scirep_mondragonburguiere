% ===================================================================
% This script creates the benchmarks described in 
% "Bio-inspired benchmark generator of extracellular multi-unit recordings".   
% It includes the integration of neural signals (simulations) + Lf +
% artifacts

% To select different experiments uncomment/comment the Parameters_XX file


% Lizbeth M.G. Dec. 2015
% ===================================================================
clear all 
clc
close all
%% *******************************************************************
%
%                   Set of Parameters: Select parameters file 
%
% *******************************************************************
fprintf('\n * Downloading parameters ...'); 

% Uncomment/Comment the paramater file desired 
Parameters_01 % Load Parameters_Nb_expeirment.m file 

fprintf('Done! \n');

%% *******************************************************************
%
%                  Downloading Files 
% 
% *******************************************************************
fprintf(' * Downloading files from selected experiment...'); 

% -> Anesth Simulation (AnS)
[Vm_AnS, t_AnS, sr_AnS, spikes_AnS, param_AnS] = GetSim (folder_AnS, window_AnS, elec_AnS); 
% -> Real Recording Anesth hc-1
[Vm_AnR_raw, t_AnR, sr_AnR] = Gethc1 (file_AnR, window_AnR, elec_AnR);
[ Vm_AnR_mv ] = hpFilter( Vm_AnR_raw, sr_AnR, fc_AnR );
Vm_AnR = Vm_AnR_mv*1000; %in uV now

% -> Awaken Simulation (AwS)
[Vm_AwS, t_AwS, sr_AwS, spikes_AwS, param_AwS] = GetSim (folder_AwS, window_AwS, elec_AwS); 
% -> Real Recording Awaken hc-2
[Vm_AwR_raw, t_AwR, sr_AwR] = Gethc2 (file_AwR, window_AwR, elec_AwR);
[ Vm_AwR_mv ] = hpFilter( Vm_AwR_raw, sr_AwR, fc_AwR );
Vm_AwR = Vm_AwR_mv*1000;

fprintf('Done! \n');


%% *******************************************************************
%
%          * Adding artifacts from DB*
%
% ********************************************************************
fprintf(' * Adding artifacts...'); 

samples = load('artifacts_preparedDB.mat'); % Loading database

% Anesth Simulation
conf_AnS.samp_rate = sr_AnS;
conf_AnS.artf_rate = artf_rate_AnS ; % A/1s for AnS
[Vm_AnS_A, artifacts_AnS_A] = addArtefacts_distribution(Vm_AnS, samples.preparedDB, conf_AnS, mode_art);

% Awaken Simulations
conf_AwS.samp_rate = sr_AwS;

conf_AwS.artf_rate = artf_rate_AwS ;   % A/s for AwS
[Vm_AwS_A, artifacts_AwS_A] = addArtefacts_distribution(Vm_AwS, samples.preparedDB, conf_AwS, mode_art);

fprintf('Done! *\n');

%% *******************************************************************
%
%           * Getting low frequencies from other traces *
%
% ********************************************************************
     
fprintf(' * Getting low frequency components for traces...'); 

% Low frequency components for Anesth Simulations
[Lf_signal2] = GetLowFrequenciesHc1(file_AnR , window_lf_AnR, elec_AnR, fcL_AnR); 
[ Vm_AnS_lf_A ] = SetLowFreq2Sim( Lf_signal2, sr_AnR, (Vm_AnS_A), sr_AnS ); % nb A/s for AnS

% Low frequency components for Awaken Simulations
[Lf_signal1] = GetLowFrequencies(file_AwR, window_lf_AwR, elec_AwR, fcL_AwR); 
[ Vm_AwS_lf_A ] = SetLowFreq2Sim( Lf_signal1, sr_AwR, (Vm_AwS_A), sr_AwS);  % nb A/s for AwS

fprintf('Done! *\n');


% Saving the files
% save Vm_AnS_lf_A.mat
% save Vm_AwS_lf_A.mat


